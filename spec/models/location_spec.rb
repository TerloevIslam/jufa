# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Location, type: :model do
  subject do
    described_class.new(postal: 86899,
                        adress: 'Am Ziegelanger 2')
  end

  #VALIDATIONS
  describe 'validations' do
    it 'is valid with valid attributes' do
      expect(subject).to be_valid
    end

    it 'is not valid without postal or adress' do
      %i[postal adress].each do |attr|
        subject[attr] = nil

        expect(subject).not_to be_valid
      end
    end

    describe 'office' do
      it 'is not valid without name' do
        subject.location_type = 1
        subject.name = nil

        expect do
          subject.save!
        end.to raise_error(ActiveRecord::RecordInvalid)
      end
    end
  end

  #BEFORE VALIDATIONS
  describe 'before validations' do
    it 'generates city based on postal' do
      subject.save!

      expect(subject.city).to eq("Landsberg am Lech")
    end

    describe 'office' do
      it 'generates authentication' do
        subject.location_type = 1
        subject.name = "Kanzlei"
        subject.save!

        expect(subject.authentication).to be_present
        expect(subject.authentication).to be_a(String)
      end
    end
  end
end
