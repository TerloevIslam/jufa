# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Paralegal, type: :model do
  subject { build(:paralegal) }

  #VALIDATIONS
  describe 'validations' do
    before do
      location = create(:location, :office)
      subject.location = location
    end

    it 'is valid with valid attributes' do
      expect(subject).to be_valid
    end

    it 'is not valid without location attributes' do
      subject.location = nil

      expect(subject).not_to be_valid
    end
  end
end
