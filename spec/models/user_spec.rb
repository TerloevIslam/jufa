# frozen_string_literal: true

require 'rails_helper'

RSpec.describe User, type: :model do
  subject do
    described_class.new(email: 'example@mail.com',
                        first_name: 'Islam',
                        last_name: 'Terloev',
                        birthday: Time.zone.today - 25.years,
                        password: '123456',
                        type: ['Lawyer', 'Client', 'Paralegal'].sample,
                        gender: 'male')
  end

  describe 'validations' do
    before do
      location = create(:location)
      subject.location = location
    end

    it 'is valid with valid attributes' do
      expect(subject).to be_valid
    end

    it 'is not valid without email, first_name, last_name, birthday, type or gender' do
      %i[email first_name last_name birthday type gender].each do |attr|
        subject[attr] = nil

        expect(subject).not_to be_valid
      end
    end
  end
end
