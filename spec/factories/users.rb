# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    email { Faker::Internet.safe_email }
    first_name { Faker::Name.name }
    last_name { Faker::Name.name }
    birthday { "21/01/1990" }
    password { 'password' }
    gender { ['male', 'female'].sample }

    factory :paralegal, class: Paralegal do
      type { 'Paralegal' }
    end

    factory :lawyer, class: Lawyer do
      type { 'Lawyer' }
    end

    factory :client, class: Client do
      type { 'Client' }
    end
  end
end
