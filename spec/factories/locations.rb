# frozen_string_literal: true

FactoryBot.define do
  factory :location do
    postal { Faker::Address.zip }
    adress { 'Reichelstraße 3' }
    location_type { 'home' }

    trait :office do
      name { Faker::Company.name }
      authentication { Faker::Number.hexadecimal(8) }
      location_type { 'office' }
    end
  end
end
