# frozen_string_literal: true

class Lawyer < User
  belongs_to :location
  accepts_nested_attributes_for :location, reject_if: :all_blank
  has_and_belongs_to_many :paralegals,
                          class_name: 'Paralegal',
                          join_table: :lawyers_paralegals,
                          foreign_key: :lawyer_id

  def location_attributes=(location_attributes)
    location_attributes[:location_type] = 1
    postal = location_attributes[:postal]
    authentication = location_attributes[:authentication]

    self.location =
      if [authentication, postal].all?(&:present?)
        Location.find_by(postal: postal, authentication: authentication)
      else
        Location.create(location_attributes)
      end
  end
end
