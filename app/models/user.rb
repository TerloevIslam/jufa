# frozen_string_literal: true

class User < ApplicationRecord
  enum status: %i[active inactive]
  GENDER = %w[male female].freeze

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  validates :email, :first_name, :last_name, :birthday, :gender, presence: true
  validates :gender, inclusion: { in: GENDER }

  before_save do
    first_name.capitalize!
    last_name.capitalize!
  end
end
