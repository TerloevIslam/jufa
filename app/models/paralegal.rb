# frozen_string_literal: true

class Paralegal < User
  belongs_to :location
  accepts_nested_attributes_for :location, reject_if: :all_blank
  has_and_belongs_to_many :lawyers,
                          class_name: 'Lawyer',
                          join_table: :lawyers_paralegals,
                          foreign_key: :paralegal_id

  def location_attributes=(location_attributes)
    locations = Location.where(location_type: 1)
    self.location = locations.find_by(location_attributes)
  end
end
