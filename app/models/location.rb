# frozen_string_literal: true

class Location < ApplicationRecord
  enum location_type: %i[home office]

  geocoded_by :postal
  after_validation :geocode, if: :postal_change

  has_many :clients
  has_many :lawyers
  has_many :paralegals

  validates :postal, :adress, presence: true
  validates :name, presence: true, if: :office?, on: :create

  before_validation do
    self[:city] = Geocoder.search("DE, #{postal}").map(&:city).join
    self[:authentication] = SecureRandom.hex(4) if office?
  end
end
