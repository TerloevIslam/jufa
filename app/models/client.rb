# frozen_string_literal: true

class Client < User
  belongs_to :location
  accepts_nested_attributes_for :location, reject_if: :all_blank

  def location_attributes=(location_attributes)
    location = Location.where(location_type: 0)
    self.location = location.find_or_create_by(location_attributes)
  end
end
