$(document).ready(function() {
  $('input[id="toggle_location_params"]').click(function () {
    if ($('#toggle_location_params').is(':checked')) {
      $("#location_new_tag").hide();
      $("#location_exists_tag").show();
    } else {
      $("#location_new_tag").show();
      $("#location_exists_tag").hide();
    }
  });
});