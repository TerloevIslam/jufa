# frozen_string_literal: true

class LocationsController < ApplicationController
  private

  def locations_params
    params.fetch(:location, {}).permit(:location_type, :city, :adress, :name, :authentication, :latitude, :longitude)
  end
end
