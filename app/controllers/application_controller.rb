# frozen_string_literal: true

class ApplicationController < ActionController::Base
  devise_group :user, contains: %i[client lawyer paralegal]
  before_action :authenticate_user!
  before_action :configure_permitted_parameters, if: :devise_controller?

  protected

  def configure_permitted_parameters
    added_attrs = [
      :email, :password, :password_confirmation, :remember_me,
      :first_name, :last_name, :birthday, :gender, :type, :location_id,
      { location_attributes: %i[postal authentication name adress] }
    ]
    devise_parameter_sanitizer.permit :sign_up, keys: added_attrs
    devise_parameter_sanitizer.permit :account_update, keys: added_attrs
  end
end
