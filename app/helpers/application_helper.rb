# frozen_string_literal: true

module ApplicationHelper
  def user_type(user)
    user.type.parameterize
  end

  def postal
    request.env['ipinfo'].postal
  end

  def city
    request.env['ipinfo'].city
  end
end
