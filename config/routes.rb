# frozen_string_literal: true

Rails.application.routes.draw do
  get '/home', to: 'home#index'
  #DEVISE
  devise_for :users, controllers: { sessions: 'sessions' }, skip: :registrations
  devise_for :clients, :lawyers, :paralegals, skip: :sessions, controllers: { registrations: 'registrations' }

  devise_scope :user do
    authenticated :user do
      root 'home#index', as: :authenticated_root
    end

    unauthenticated do
      root 'devise/sessions#new', as: :unauthenticated_root
    end
  end
  #LOCATIONS
  resources :locations, only: :create

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
