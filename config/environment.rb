# frozen_string_literal: true

# Load the Rails application.
require_relative 'application'

# Initialize the Rails application.
Rails.application.initialize!

# Set default Date Format
Date::DATE_FORMATS[:default] = "%d/%m/%Y"