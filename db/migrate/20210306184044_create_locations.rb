# frozen_string_literal: true

class CreateLocations < ActiveRecord::Migration[6.0]
  def change
    create_table :locations do |t|
      t.integer :location_type
      t.string :city
      t.text :adress

      t.timestamps
    end
  end
end
