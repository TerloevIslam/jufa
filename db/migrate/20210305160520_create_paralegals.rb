# frozen_string_literal: true

class CreateParalegals < ActiveRecord::Migration[6.0]
  def change
    create_table :paralegals, &:timestamps
  end
end
