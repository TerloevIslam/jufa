class UpdateLocations2 < ActiveRecord::Migration[6.0]
  def change
    remove_column :locations, :lawyers_id
    remove_column :locations, :paralegals_id
    change_column :locations, :authentication, :string, null: true
  end
end
