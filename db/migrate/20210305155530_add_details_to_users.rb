# frozen_string_literal: true

class AddDetailsToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :first_name, :string
    add_column :users, :last_name, :string
    add_column :users, :birthday, :date
    add_column :users, :adress, :text
    add_column :users, :sex, :integer
    add_column :users, :status, :integer, default: 0
    add_column :users, :type, :string
  end
end
