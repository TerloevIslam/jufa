class AddPostalToLocations < ActiveRecord::Migration[6.0]
  def change
    add_column :locations, :postal, :integer
  end
end
