# frozen_string_literal: true

class LawyersParalegalsAssociations < ActiveRecord::Migration[6.0]
  def self.up
    create_table :lawyers_paralegals, id: false do |t|
      t.integer :lawyer_id
      t.integer :paralegal_id
    end

    add_index(:lawyers_paralegals, %i[lawyer_id paralegal_id])
    add_index(:lawyers_paralegals, %i[paralegal_id lawyer_id])
  end

  def self.down
    remove_index(:lawyers_paralegals, %i[lawyer_id paralegal_id])
    remove_index(:lawyers_paralegals, %i[paralegal_id lawyer_id])
    drop_table :lawyers_paralegals
  end
end
