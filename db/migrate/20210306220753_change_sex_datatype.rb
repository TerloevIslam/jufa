# frozen_string_literal: true

class ChangeSexDatatype < ActiveRecord::Migration[6.0]
  def change
    change_column :users, :sex, :string
  end
end
