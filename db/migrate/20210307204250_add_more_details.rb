# frozen_string_literal: true

class AddMoreDetails < ActiveRecord::Migration[6.0]
  def change
    add_column :locations, :name, :string, default: nil
    add_reference :users, :location, foreign_key: true
  end
end
