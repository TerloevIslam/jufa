class UpdateLocations < ActiveRecord::Migration[6.0]
  def change
    rename_column :locations, :pin, :authentication
    rename_column :users, :sex, :gender
    remove_index :locations, name: :index_locations_on_lawyers_id
    remove_index :locations, name: :index_locations_on_paralegals_id
  end
end
